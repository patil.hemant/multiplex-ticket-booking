class Theatre < ApplicationRecord
	has_many :seat_types
	has_many :time_slots
	has_many :screens
end
