class Movie < ApplicationRecord
	serialize :star_casts, Hash
	mount_uploader :banner, BannerUploader

	has_many :shows
end
