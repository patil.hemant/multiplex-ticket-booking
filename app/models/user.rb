class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :bookings, foreign_key: :customer_id

  after_create :set_role

  def set_role
    self.update_column('role', 'Customer') if self.role != 'Admin'
  end

   def admin?
   	 role == 'Admin'
   end

   def customer?
   		role == 'Customer'
   end
end
