class CreateSeats < ActiveRecord::Migration[5.2]
  def change
    create_table :seats do |t|
      t.string :seat_no
      t.integer :row_id
      t.integer :seat_type_id

      t.timestamps
    end
    add_index :seats, :row_id
    add_index :seats, :seat_type_id
  end
end
